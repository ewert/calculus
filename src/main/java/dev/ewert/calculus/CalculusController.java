/*
 * Copyright 2019 Marc Ewert
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ewert.calculus;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

/**
 * Controller offering the endpoints of the calculus application.
 */
@Validated
@RestController
public class CalculusController {

    private final CalculusService calculusService;

    public CalculusController(CalculusService calculusService) {
        this.calculusService = calculusService;
    }

    /**
     * Returns the results of the calculation of the given input string.
     * @param input Base64 encoded input string containing a calculation like "1 + 2 * 3".
     * @return The result of the calculation.
     */
    @GetMapping("calculus")
    public CalculatorResponse calculate(@RequestParam("query") @NotBlank String input) {
        return new CalculatorResponse(calculusService.calculateFromBase64(input));
    }
}

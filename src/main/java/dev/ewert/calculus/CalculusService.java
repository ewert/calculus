/*
 * Copyright 2019 Marc Ewert
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ewert.calculus;

import dev.ewert.calculus.calculator.Calculator;
import dev.ewert.calculus.calculator.CalculatorException;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

/**
 * Service for performing the calculations.
 */
@Service
public class CalculusService {

    private final Calculator calculator;

    public CalculusService(Calculator calculator) {
        this.calculator = calculator;
    }

    /**
     * Returns the result for the given calculation.
     * @param encoded Base64 encoded calculation string.
     * @return The result of the calculation.
     */
    public double calculateFromBase64(String encoded) {
        String text;
        try {
            text = new String(Base64Utils.decodeFromString(encoded));
        } catch (Exception e) {
            throw new CalculatorException("Failed to decode base64 input", e);
        }
        return calculator.calculate(text);
    }
}

/*
 * Copyright 2019 Marc Ewert
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ewert.calculus.calculator;

import dev.ewert.calculus.antlr.CalculusBaseVisitor;
import dev.ewert.calculus.antlr.CalculusParser;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Visitor pattern for the parsed calculation tree. Each node resolves
 * to a double value.
 */
public class CalculusVisitor extends CalculusBaseVisitor<Double> {

    @Override
    public Double visitAddition(CalculusParser.AdditionContext ctx) {
        return visit(ctx.expression()) + visit(ctx.mulOrDivExpression());
    }

    @Override
    public Double visitSubtraction(CalculusParser.SubtractionContext ctx) {
        return visit(ctx.expression()) - visit(ctx.mulOrDivExpression());
    }

    @Override
    public Double visitMultiplication(CalculusParser.MultiplicationContext ctx) {
        return visit(ctx.mulOrDivExpression()) * visit(ctx.primaryExpression());
    }

    @Override
    public Double visitDivision(CalculusParser.DivisionContext ctx) {
        Double divisor = visit(ctx.primaryExpression());
        if (divisor == 0) {
            // For division by zero protection it's okay to compare double with zero
            throw new CalculatorException("Detected division by zero");
        }
        return visit(ctx.mulOrDivExpression()) / divisor;
    }

    @Override
    public Double visitToPlus(CalculusParser.ToPlusContext ctx) {
        return +visit(ctx.primaryExpression());
    }

    @Override
    public Double visitToMinus(CalculusParser.ToMinusContext ctx) {
        return -visit(ctx.primaryExpression());
    }

    @Override
    public Double visitBracketed(CalculusParser.BracketedContext ctx) {
        return visit(ctx.expression());
    }

    @Override
    public Double visitNumber(CalculusParser.NumberContext ctx) {
        try {
            return DecimalFormat.getNumberInstance(Locale.US)
                .parse(ctx.getText()).doubleValue();
        } catch (ParseException e) {
            throw new CalculatorException("Illegal number: " + ctx.getText(), e);
        }
    }

    @Override
    public Double visitInput(CalculusParser.InputContext ctx) {
        return visit(ctx.expression());
    }
}

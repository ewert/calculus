/*
 * Copyright 2019 Marc Ewert
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ewert.calculus.calculator;

import dev.ewert.calculus.antlr.CalculusLexer;
import dev.ewert.calculus.antlr.CalculusParser;
import org.antlr.v4.runtime.*;
import org.springframework.stereotype.Service;

/**
 * The Calculator is able to parse a given calculation string and return
 * the result as a double.
 */
@Service
public class Calculator {

    private final CalculusVisitor visitor = new CalculusVisitor();
    private final BaseErrorListener errorListener = new BaseErrorListener() {
        @Override
        public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
            throw new CalculatorException("line " + line + ":" + charPositionInLine + " " + msg, e);
        }
    };

    /**
     * Parses the given input String in the form of "1 + 2 * 3" and returns the result
     * as a double. Will throw a CalculatorException in case of errors.
     * @param text Input string describing a calculation with the operations +, -, * and /,
     *             parenthesis and integer resp decimal numbers in the form of "12.4".
     * @return The result of the calculation.
     */
    public double calculate(String text) {
        CalculusLexer lexer = new CalculusLexer(CharStreams.fromString(text));
        CalculusParser parser = new CalculusParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.addErrorListener(errorListener);
        return visitor.visit(parser.input());
    }
}

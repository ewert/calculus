/*
 * Copyright 2019 Marc Ewert
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

grammar Calculus;

input: expression EOF;

expression
  : expression ADD mulOrDivExpression # Addition
  | expression SUB mulOrDivExpression # Subtraction
  | mulOrDivExpression                # MulOrDiv
  ;

mulOrDivExpression
  : mulOrDivExpression MUL primaryExpression # Multiplication
  | mulOrDivExpression DIV primaryExpression # Division
  | primaryExpression                        # Primary
  ;

primaryExpression
  : ADD primaryExpression    # ToPlus
  | SUB primaryExpression    # ToMinus
  | LPAREN expression RPAREN # Bracketed
  | constExpression          # Constant
  ;

constExpression
  : NUMBER # Number
  ;

LPAREN: '(';
RPAREN: ')';
ADD:    '+';
SUB:    '-';
MUL:    '*';
DIV:    '/';
DOT:    '.';

fragment Digit: '0'..'9';
fragment Integer: Digit+;
NUMBER:  Integer (DOT Integer)?;

WS: [ \n\r\t\u000C]+ -> skip;

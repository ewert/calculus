/*
 * Copyright 2019 Marc Ewert
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ewert.calculus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = CalculusApp.class)
@AutoConfigureMockMvc
public class CalculusControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testCommonExample() throws Exception {
        mvc.perform(get("/calculus?query=MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("error", is(false)))
            .andExpect(jsonPath("result", closeTo(new BigDecimal(-132.88888888888889), new BigDecimal(0.000001))));
    }

    @Test
    public void testInvalidBase64() throws Exception {
        mvc.perform(get("/calculus?query=MiAqICg_________ogKDIqMyk")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError())
            .andExpect(jsonPath("error", is(true)))
            .andExpect(jsonPath("message", containsString("Failed to decode base64 input")));
    }

    @Test
    public void testInvalidSyntax() throws Exception {
        mvc.perform(get("/calculus?query=MSArIDIgKg==")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError())
            .andExpect(jsonPath("error", is(true)))
            .andExpect(jsonPath("message", containsString("mismatched input")));
    }
}

/*
 * Copyright 2019 Marc Ewert
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ewert.calculus.calculator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;

public class CalculatorTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    private final Calculator calculator = new Calculator();

    private static final double EPSILON = 0.0000001;

    @Test
    public void testAdd() {
        Double result = calculator.calculate("1 + 2");
        assertThat(result, closeTo(3, EPSILON));
    }

    @Test
    public void testSub() {
        Double result = calculator.calculate("1 - 2");
        assertThat(result, closeTo(-1, EPSILON));
    }

    @Test
    public void testMul() {
        Double result = calculator.calculate("1 * 2");
        assertThat(result, closeTo(2, EPSILON));
    }

    @Test
    public void testDiv() {
        Double result = calculator.calculate("1 / 2");
        assertThat(result, closeTo(0.5, EPSILON));
    }

    @Test
    public void testAddAndMul() {
        Double result = calculator.calculate("1 + 2 * 3");
        assertThat(result, closeTo(7, EPSILON));
    }

    @Test
    public void testMulAndAdd() {
        Double result = calculator.calculate("1 * 2 + 3");
        assertThat(result, closeTo(5, EPSILON));
    }

    @Test
    public void testParenthesis() {
        Double result = calculator.calculate("(1 + 2) * 3");
        assertThat(result, closeTo(9, EPSILON));
    }

    @Test
    public void testMultipleParenthesis() {
        Double result = calculator.calculate("(((1 + 2))) * (3)");
        assertThat(result, closeTo(9, EPSILON));
    }

    @Test
    public void testDecimal() {
        Double result = calculator.calculate("(1.2 + 2.7) * 2.55 * 0.3");
        assertThat(result, closeTo(2.9835, EPSILON));
    }

    @Test
    public void testNoSpaces() {
        Double result = calculator.calculate("(1.2+2.7)*2.55*0.3");
        assertThat(result, closeTo(2.9835, EPSILON));
    }

    @Test
    public void testManySpaces() {
        Double result = calculator.calculate("(1.2    +\t\n2.7) *    2.55\t  *\n0.3");
        assertThat(result, closeTo(2.9835, EPSILON));
    }

    @Test
    public void testSimpleDecimal() {
        Double result = calculator.calculate("56.456");
        assertThat(result, closeTo(56.456, EPSILON));
    }

    @Test
    public void testComplexExample() {
        Double result = calculator.calculate("2 * (23/(3*3))- 23 * (2*3)");
        assertThat(result, closeTo(-132.88888888, EPSILON));
    }

    @Test
    public void testDivideByZero() {
        thrown.expect(CalculatorException.class);
        thrown.expectMessage("Detected division by zero");
        calculator.calculate("1/0");
    }

    @Test
    public void testDivideByZeroDecimal() {
        thrown.expect(CalculatorException.class);
        thrown.expectMessage("Detected division by zero");
        calculator.calculate("1/0.000");
    }

    @Test
    public void testMissingParen() {
        thrown.expect(CalculatorException.class);
        thrown.expectMessage(" missing ')'");
        calculator.calculate("(1+ 2");
    }

    @Test
    public void testMissingExpression() {
        thrown.expect(CalculatorException.class);
        thrown.expectMessage("mismatched input");
        calculator.calculate("1 + 2 -");
    }
}

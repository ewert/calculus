FROM openjdk:12-jdk-oracle as build
COPY ./ /app
WORKDIR /app
RUN ./gradlew bootJar


FROM openjdk:12-jdk-oracle
COPY --from=build /app/build/libs/app.jar /app/app.jar
RUN chown -R 1000000:1000000 /app
EXPOSE 8080
USER 1000000:1000000

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/app.jar"]

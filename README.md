# Calculus

Calculus is a simple Java Spring Boot demo application showcasing the usage of Spring Boot and
ANTLR offering a simple calculator.

The calculator can be accessed through a GET request on the endpoint `/calculus?query=input`, where
input is a UTF8 Base64 encoded string like `"MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk"` containing 
the calculation.  

The result looks in successful cases like `{error: false, result: number}` and in case of errors
like `{error: true, message: string}`.

Allowed operations in the calculator are `+ - * / ( )` the numbers has to be integers or simple decimals
in the form of `23.45`.

By using the parser generator ANTLR the calculator could be easily enhanced with further functionality. The 
[grammar](src/main/antlr/Calculus.g4) could be extended and the 
[visitor](src/main/java/dev/ewert/calculus/calculator/CalculusVisitor.java) would be needed to be 
adapted accordingly.

The calculus application is automatically deployed to AWS as a Docker image defined by
this [Dockerfile](Dockerfile) through the 
[gitlab-ci.yml](.gitlab-ci.yml) file after changes on the master branch. On the target system a simple deploy
script will pull the image and replace the running app. 

```
#!/bin/sh

IMAGE=$1

docker pull $IMAGE
docker stop calculus || true && docker rm calculus || true
docker run --name calculus --restart always -p 80:8080 -d $IMAGE
``` 

Calculus is available
on an [AWS EC2 instance](http://platfish.org/calculus?query=MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk). There is also a [Swagger UI](http://platfish.org/swagger-ui.html) 
visualizing the offered API.

Source for project icon: https://de.freeimages.com/photo/calculate-1240498